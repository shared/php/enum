<?php

declare(strict_types=1);

namespace Naker\Enum;

/**
 * @author  Iqbal Maulana <iq.bluejack@gmail.com>
 */
trait EnumHelper
{
    public static function values(): array
    {
        return array_map(fn(self $enum) => $enum->value, self::cases());
    }

    public function is($enum): bool
    {
        return $this === $enum || $this->value === $enum;
    }
}
